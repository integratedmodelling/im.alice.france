private namespace alice.france.estuarine;

quality Richness is count of biology:Species ecology:Population;


model local:stefano.balbi:im.alice.france:estuary_conv
 as value of landcover:Estuary IUPAC:Water im:Volume during earth:Summer;
// 	observing 
//        im:Volume of landcover:Estuary earth:Site in m^3 named estuary_volume
//        using gis.points.collect(aggregate = estuary_volume, radius = 500, circular = true); 

/*streamflow */
number estuary_streamflow
       observing 
       @intensive(space) IUPAC:Water im:Volume during earth:Summer in mm named stream_flow,  
       value of landcover:Estuary IUPAC:Water im:Volume during earth:Summer named conv
       set to [def x = ( conv >=0 && conv <=500 && stream_flow > 0) ? stream_flow : unknown
       	       return x];

@intensive(space) 
model landcover:Estuary IUPAC:Water im:Volume during earth:Summer in mm named stream_flow_at_estuary
     observing 
     im:Volume of landcover:Estuary earth:Site in hm^3 named estuary_volume,
     estuary_streamflow
     set to [ def y = (estuary_volume > 0) ? estuary_volume : unknown
     	      def x = (y <= 500) ? estuary_streamflow.max :   unknown
     	      def z = nodata(y) ? unknown : x
     	      return z];
/*SS */
number estuary_ss
	observing 
	ratio of (chemistry:Suspension physical:Solid im:Mass during earth:Winter) to (im:Area of hydrology:Watershed) in kg named ss,
	value of landcover:Estuary IUPAC:Water im:Volume during earth:Summer named conv
       set to [def x = ( conv >=0 && conv <=500 && ss > 0) ? ss : unknown
       	       return x];

model landcover:Estuary ratio of (chemistry:Suspension physical:Solid im:Mass during earth:Winter) to (im:Area of hydrology:Watershed) in kg named solid_suspension_load_at_estuary_during_winter
	observing 
	im:Volume of landcover:Estuary earth:Site in hm^3 named estuary_volume,
     estuary_ss
     set to [ def y = (estuary_volume > 0) ? estuary_volume : unknown
     	      def x = (y <= 500) ? estuary_ss.max :   unknown
     	      def z = nodata(y) ? unknown : x
     	      return z];

/*NO3 */

number estuary_no3
	observing 
	ratio of (chemistry.incubation:Nitrate im:Mass) to (im:Area of hydrology:Watershed) in kg named no3,
	value of landcover:Estuary IUPAC:Water im:Volume during earth:Summer named conv
       set to [def x = ( conv >=0 && conv <=500 && no3 > 0) ? no3 : unknown
       	       return x];

model landcover:Estuary ratio of (chemistry.incubation:Nitrate im:Mass) to (im:Area of hydrology:Watershed) in kg named nitrate_load_at_estuary
	observing 
	im:Volume of landcover:Estuary earth:Site in hm^3 named estuary_volume,
     estuary_no3
     set to [ def y = (estuary_volume > 0) ? estuary_volume : unknown
     	      def x = (y <= 500) ? estuary_no3.max :   unknown
     	      def z = nodata(y) ? unknown : x
     	      return z]; 
     	      
/*PO4 */

number estuary_po4
	observing 
	ratio of (chemistry.incubation:Phosphate im:Mass) to (im:Area of hydrology:Watershed) in kg named po4,
	value of landcover:Estuary IUPAC:Water im:Volume during earth:Summer named conv
       set to [def x = ( conv >=0 && conv <=500 && po4 > 0) ? po4 : unknown
       	       return x];

model landcover:Estuary ratio of (chemistry.incubation:Phosphate im:Mass) to (im:Area of hydrology:Watershed) in kg named phosphate_load_at_estuary
	observing 
	im:Volume of landcover:Estuary earth:Site in hm^3 named estuary_volume,
     estuary_po4
     set to [ def y = (estuary_volume > 0) ? estuary_volume : unknown
     	      def x = (y <= 500) ? estuary_po4.max :   unknown
     	      def z = nodata(y) ? unknown : x
     	      return z];  


/*BN models */

@intensive(space)  
model local:stefano.balbi:alice.sandbox:bn.estuarine.species2
		as value of landcover:Estuary ecology.incubation:Benthic Richness
		observing 
		im:Volume of landcover:Estuary earth:Site in hm^3                 named volume_of_estuary_site ,
		landcover:Estuary IUPAC:Water im:Volume during earth:Summer in mm named water_volume_during_summer;  

@intensive(space)
model local:stefano.balbi:alice.sandbox:bn.estuarine.ssc2
		as landcover:Estuary ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) during earth:Winter in mg/L                   
		observing 
		 im:Volume of landcover:Estuary earth:Site in hm^3 named volume_of_estuary_site,
		 landcover:Estuary ratio of (chemistry:Suspension physical:Solid im:Mass during earth:Winter) to (im:Area of hydrology:Watershed) in kg named solid_suspension_mass_during_winter_to_area_of_watershed_ratio,
		 landcover:Estuary IUPAC:Water im:Volume during earth:Winter in mm named water_volume_during_winter;  
		                   
@intensive(space)
model local:stefano.balbi:alice.sandbox:bn.estuarine.opportunism2 as
		proportion of (count of ecology:Population with ecology:Commensalism) in (count of ecology.incubation:Benthic ecology:Population) 
	    observing
		im:Volume of landcover:Estuary earth:Site in hm^3 named volume_of_estuary_site,
		landcover:Estuary ratio of (chemistry.incubation:Nitrate im:Mass) to (im:Area of hydrology:Watershed) in kg named nitrate_mass_to_area_of_watershed_ratio,
		landcover:Estuary ratio of (chemistry.incubation:Phosphate im:Mass) to (im:Area of hydrology:Watershed) in kg named phosphate_mass_to_area_of_watershed_ratio;
				