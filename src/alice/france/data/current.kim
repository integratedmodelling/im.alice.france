namespace alice.france.data.current;


//precipitation
@intensive(space)
model 'local:amarquez:im.alice.france:france_precipitation_2010_2012'
	as earth:PrecipitationVolume in mm named prec; 

@intensive(space)
//model 'local:amarquez:im.alice.france:FR_prec_annual_accum_2010_2012'
//	as earth:PrecipitationVolume in mm named prec;

@intensive(space)
model 'local:amarquez:im.alice.france:france_precipitation_dryseason_2010_2012'
	as earth:PrecipitationVolume during earth:Dry im:Season in mm;
	
//temp (min-mean-max)	

model 'local:amarquez:im.alice.france:france_mean_temperature_2010_2012'
	as earth:AtmosphericTemperature in Celsius named tmed;

model 'local:amarquez:im.alice.france:france_mean_max_temperature_2010_2012'
	as im:Maximum earth:AtmosphericTemperature in Celsius;

model 'local:amarquez:im.alice.france:france_mean_max_temperature_dryseason_2010_2012'
	as im:Maximum earth:AtmosphericTemperature during earth:Dry im:Season in Celsius;

model 'local:amarquez:im.alice.france:france_mean_min_temperature_2010_2012'
	as im:Minimum earth:AtmosphericTemperature in Celsius; // named tmin;
	
model 'local:amarquez:im.alice.france:france_meantemperature_wetseason_2010_2012'
	as earth:AtmosphericTemperature during earth:Wet im:Season in Celsius;

@intensive(space)	
model 'local:amarquez:im.alice.france:france_precipitation_wetseason_2010_2012'
	as earth:PrecipitationVolume during earth:Wet im:Season in mm;

//	
//radiation
model local:stefano.balbi:im.alice.france:fr_mean_radiation_50m_wgs84
	as earth:SolarRadiation in J named rg;
	
model 'local:amarquez:im.alice.france:fr_radiation_dryseason_50m_wgs84'
	as im:Mean earth:SolarRadiation during earth:Dry im:Season in J;
//
//
 model 'local:amarquez:im.alice.france:fr_landcover_current_2018_50m_wgs84'
	as landcover:LandCoverType
		classified into
		   landcover:BroadleafForest        if 1,
		   landcover:ConiferousForest       if 2,
	       landcover:Shrubland              if 3,  
	       landcover:Grassland              if 5,
	       landcover:AgriculturalVegetation if 61,  
           //landcover:BareArea               if 7,  	      
	       landcover:ArtificialSurface      if 8, 
	       landcover:WaterBody              if 9
	       ; 


//	
//landcover detailed       
//model 'local:amarquez:im.alice.france:fr_landcover_2018detailed_50m_wgs84'
//	as landcover:LandCoverType
//	       classified into
//           landcover:BroadleafForest        if 1,
//		   landcover:ConiferousForest       if 2,
//	       landcover:Shrubland              if 3,  
//	       landcover:Grassland              if in (31, 32),
//	       landcover:AgriculturalVegetation if in (21, 22, 23), 
////         landcover:BareArea               if 7,  	      
//	       landcover:ArtificialSurface      if 8, 
//	       landcover:WaterBody              if 9
//	       ;

model 'local:stefano.balbi:im.alice.spain:bn.pastureland.primary.species.pma.new'
	as type of im:Relevant landcover:Pastureland biology:Plant    	
//     	as type of economics:Estimated im:Relevant landcover:Pastureland earth:Region 
     observing 
        landcover:LandCoverType named landcover_type,
        geography:Elevation,
        geography:Slope,
        geography:Aspect
      set to [(landcover_type is landcover:Grassland || landcover_type is landcover:Shrubland 
	        ) ? self : unknown];
	        
model 'local:stefano.balbi:im.alice.spain:bn.pastureland.overstory.species.pma'
	as type of ecology.incubation:Overstory im:Relevant landcover:Pastureland biology:Plant
	 observing 
        landcover:LandCoverType named landcover_type,
        geography:Elevation,
        geography:Slope,
        geography:Aspect
      set to [(landcover_type is landcover:Grassland || landcover_type is landcover:Shrubland 
	        ) ? self : unknown];
	       
/*
1 broadleaf forest
2 mixed forest
3 shrubland
8 artificialised
9 water
21 wheat
22 maize
23 other crops
31 permanent meadow
32 temporary meadow
 */
 
//model 'local:amarquez:im.alice.france:fr_landcover_current_2018_50m_wgs84'
//	as type of ecology:Forest ecology:LeafType 
//		classified into
//           ecology:Broadleaved          if 1,
//		   ecology:Needleleaved         if 2;	

	

//model 'local:amarquez:im.alice.france:fr_landcover_2018detailed_50m_wgs84_cattle' //Green_Plant_Biomasses MODEL
//	as count of biology:Individual agriculture:Cattle in ha;  

@intensive(space)
model 'local:amarquez:im.alice.france:fr_landcover_2018detailed_50m_wgs84_cattle' //Green_Plant_Biomasses MODEL
	as count of biology:Individual agriculture:Cattle in ha;


/*Data for the river reaches: initialization for current scenario ------------------------------*/
model local:stefano.balbi:im.alice.france:current_catch_area   as im:Area of hydrology:Watershed in km^2;  
model 'local:stefano.balbi:im.alice.france:current_BF_blf'       as proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed);  
model local:stefano.balbi:im.alice.france:current_mn_blf       as proportion of landcover:BroadleafForest in (im:Area of hydrology:Watershed);   
model local:stefano.balbi:im.alice.france:current_effluents    as im:Numerosity of im:Outgoing earth:Stream; 
model local:stefano.balbi:im.alice.france:current_mn_uhd       as proportion of landcover:Urban in (im:Area of hydrology:Watershed);
model 'local:stefano.balbi:im.alice.france:current_TMeSum'      as IUPAC:Water im:Temperature  during earth:Summer in Celsius; 
model local:stefano.balbi:im.alice.france:current_fre3         as value of IUPAC:Water im:Volume;  
model local:stefano.balbi:im.alice.france:current_acw_m       as hydrology.incubation:StreamWidth in m;  
model local:stefano.balbi:im.alice.france:current_free_length  as im:Width of ecology:Habitat earth:Region in m;
@intensive(space)   
model local:stefano.balbi:im.alice.france:current_flowsum     as IUPAC:Water im:Volume during earth:Summer in mm;	  
/*Estuarine */
model 'local:stefano.balbi:im.alice.france:france_current_Estuary_vo' 	as im:Volume of landcover:Estuary earth:Site in hm^3;
@intensive(space)
model 'local:stefano.balbi:im.alice.france:france_current_Flow_WSA'      	as landcover:Estuary IUPAC:Water im:Volume during earth:Winter in mm;  
/*agri specific */
model 'local:stefano.balbi:im.alice.france:MN_agr_current_p' as proportion of landcover:AgriculturalVegetation in (im:Area of hydrology:Watershed); 
model 'local:stefano.balbi:im.alice.france:MN_mai_current' as proportion of agriculture:Maize in (im:Area of hydrology:Watershed);